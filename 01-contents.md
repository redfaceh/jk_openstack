## 1. 클라우드와 가상화
### 1) 가상화
- 서버 가상화
- 네트워크 가상화
- 스토리지 가상화
- 컨테이너 가상화

### 2) 클라우드
- 클라우드 컴퓨팅
- 클라우드 서비스 분류
  - SaaS
  - PaaS
  - IaaS

- 클라우드 서비스 종류
  - 퍼블릭 클라우드
  - 프라이빗 클라우드
  - 하이브리드 클라우드
  - 멀티 클라우드

## 2. OpenStack 소개

### 1) OpenStack 코어 서비스

|프로젝트 이름|서비스 기능|
|---|---|
|Nova|Compute|
|Glane|Image|
|Swift|Object Storage|
|Keystone|Identity|
|Horizon|Dashborad|
|Cinder|Block Storage|
|Neutron|Networking|
|Ceilometer|Telemetry|
|Heat|Orchestration|

### 2) OpenStack 아키텍처
- OpenStack 구조
- OpenStack 노드
  - 컨트롤러 노드(Controller Node)
  - 컴퓨트 노드(Compute Node)
  - 네트워크 노드(Network Node)
  - 스토리지 노드(Storage Node)

## 3. OpenStack 대시보드 사용
### 1) admin 역할 사용
- 프로젝트
- 사용자
- 외부 네트워크
- 플레이버
- 이미지(공용)

### 2) _member_ 역할 사용
- 이미지(사설)
- 내부 네트워크
- 라우터
- 보안 그룹
- 키 페어
- 인스턴스
- 볼륨
- 오브젝트 스토리지
- 스택

## 4. OpenStack 통합 명령 사용
- 프로젝트 명령
- openstack 통합 명령
- 자격증명 파일

## 5. OpenStack ID 서비스
- 인증(Authentication)
- 권한 부여(Authorization)
- 토큰 관리
- 카탈로그

## 6. OpenStack 이미지 관리
- Glance 서비스
  - glance-api
  - glance-registry
- 이미지 포맷(raw, qcow2, vdi, vmdk, vhd, ami, ari, aki)
- 이미지 속성(public/private, protected, min-disk, min-ram)
- 백엔드 스토리지


## 7. OpenStack 네트워킹
- Neutron 서비스
  - neutron-server
  - neutron-l3-agent
  - neutron-dhcp-agent
  - neutron-metadata-agent
  - neutron-openvswitch-agent
  - neutron-metering-agent

- 네트워크 네임스페이스
- 프로바이더 네트워크
  - 가상 네트워크와 물리 네트워크를 연결
  - 관리자 역할을 사용해야 함
  - flat
  - vlan
  - vxlan
  - gre
- 프로젝트 네트워크(self-tenancy network)
  - 프로젝트 내에서 인스턴스가 사용하는 가상의 네트워크
  - 일반 사용자 역할을 사용하여 생성 가능
  - vlan
  - vxlan
  - gre
- Neutron 플러그인
  - ML2 사용하여 다수의 네트워킹 기술 사용 가능
  - Linux Bridge
  - OVS(Open vSwitch) -> OVN(Open Virtual Netowrk)
  - SRIOV
  - MacVTap
  - L2 population
  - OpenDaylight / OpenContrail(OpenSource)
- OVS 브릿지
  - br-int
  - br-ex
  - br-tun

## 8. 컴퓨트 서비스
- Nova 서비스
  - openstack-nova-api
  - openstack-nova-compute
  - openstack-nova-scheduler
  - openstack-nova-conductor

- 가상 디스크 파일
- 마이그레이션
- 백엔드 하이퍼바이저
 

## 9. 블록 스토리지
- Cinder 서비스
  - openstack-cinder-api
  - openstack-cinder-scheduler
  - openstack-cinder-volume
  - openstack-cinder-backup

- 백엔드 스토리지
- 볼륨 스냅샷
- 부팅 가능한 볼륨
- 백업 및 복원

## 10. 오브젝트 스토리지
- Swift 서비스
  - siwft-proxy
  - swift-account
  - swift-container
  - swift-object

- Swift 구조
- Ceph 스토리지 소개

## 11. 원격 미터링 서비스
- Ceilometer 서비스
  - aodh
  - gnocchi

- Ceilometer 구조

## 12. OpenStack 오케스트레이션
- Heat 서비스
  - openstack-heat-api
  - openstack-heat-api-cfn
  - openstack-heat-engine

## 13. 오픈스택 설치
