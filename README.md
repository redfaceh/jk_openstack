## 1. 클라우드와 가상화
### 1) 가상화
### 2) 클라우드

## 2. OpenStack 소개
### 1) OpenStack 코어 서비스
### 2) OpenStack 아키텍처
## 3. OpenStack 대시보드 사용
### 1) admin 역할 사용
### 2) _member_ 역할 사용
## 4. OpenStack 통합 명령 사용
## 5. OpenStack ID 서비스
## 6. OpenStack 이미지 관리
## 7. OpenStack 네트워킹
## 8. 컴퓨트 서비스
## 9. 블록 스토리지
## 10. 오브젝트 스토리지
## 11. 원격 미터링 서비스
## 12. OpenStack 오케스트레이션
## 13. 오픈스택 설치
