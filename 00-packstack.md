## 1. 노드 준비
### 1) 환경
|항목|설명|
|---|---|
|하이퍼바이저|VirtualBox 6.1.6 + Extension Pack|
|네트워크1|NatNetwork(10.0.2.0/24) - 무작위 모드|
|네트워크2|Host-Only Network(192.168.56.0/24) - 무작위 모드|

### 2) 컨트롤러 노드(네트워크 및 스토리지 노드 포함)
|하드웨어|사양|
|---|---|
|CPU|2~4|
|RAM|8~12|
|NIC|1Gbps * 2|
|Disk|100G|

### 3) 컴퓨트 노드
|하드웨어|사양|
|---|---|
|CPU|2|
|RAM|4|
|NIC|1Gbps * 2|
|Disk|100G|

## 2. 기본 설정
### 1) 호스트네임 설정
- 컨트롤러 노드
```bash
root# hostnamectl set-hostname controller.redface.com
```

- 컴퓨트 노드
```bash
root# hostnamectl set-hostname compute.redface.com
```

### 2) NetworkManager 및 firewalld 서비스 비활성화
- 컨트롤러 노드
```bash
root# systemctl stop NetworkManager firewalld 
```

```bash
root# systemctl disable NetworkManager firewalld 
```

- 컴퓨트 노드
```bash
root# systemctl stop NetworkManager firewalld 
```

```bash
root# systemctl disable NetworkManager firewalld 
```


### 3) IP 설정
- 컨트롤러 노드
```bash
root# cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
DEVICE=enp0s3
ONBOOT=yes
BOOTPROTO=static
IPADDR=10.0.2.10
NETMASK=255.255.255.0
GATEWAY=10.0.2.1
DNS1=8.8.8.8
```

```bash
root# cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
DEVICE=enp0s8
ONBOOT=yes
BOOTPROTO=static
IPADDR=192.168.56.10
NETMASK=255.255.255.0
```

```bash
root# systemctl restart network
```

- 컴퓨트 노드
```bash
root# cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
DEVICE=enp0s3
ONBOOT=yes
BOOTPROTO=static
IPADDR=10.0.2.11
NETMASK=255.255.255.0
GATEWAY=10.0.2.1
DNS1=8.8.8.8
```

```bash
root# cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
DEVICE=enp0s8
ONBOOT=yes
BOOTPROTO=static
IPADDR=192.168.56.11
NETMASK=255.255.255.0
```

```bash
root# systemctl restart network
```


### 4) /etc/hosts 파일 수정

- 컨트롤러 노드


## 2. 설치
- 컨트롤러 노드

### 1) 저장소 연결
```bash
root# yum -y install centos-release-openstack-stein
```

### 2) packstack 설치
```bash
root# yum -y install openstack-packstack
```

### 3) 응답 파일 생성
```bash
root# packstack --gen-answer-file /root/answers.txt
```

```bash
root# vi /root/answers.txt
...
CONFIG_DEFAULT_PASSWORD=dkagh1.
...
CONFIG_HEAT_INSTALL=y
...
CONFIG_NTP_SERVERS=kr.pool.ntp.org
...
CONFIG_COMPUTE_HOSTS=10.0.2.11
...
CONFIG_KEYSTONE_ADMIN_PW=dkagh1.
...
CONFIG_NEUTRON_ML2_TYPE_DRIVERS=flat,vxlan
...
CONFIG_NEUTRON_ML2_TENANT_NETWORK_TYPES=vxlan
...
CONFIG_NEUTRON_ML2_MECHANISM_DRIVERS=openvswitch
...
CONFIG_NEUTRON_L2_AGENT=openvswitch
...
CONFIG_NEUTRON_OVS_BRIDGE_IFACES=br-ex:enp0s3
...
CONFIG_NEUTRON_OVS_TUNNEL_IF=enp0s8
...
CONFIG_NEUTRON_OVS_TUNNEL_SUBNETS=192.168.56.0/24
...
CONFIG_PROVISION_DEMO=n
```

### 4) 응답 파일 실행
```bash
root# packstack --answer-file /root/answers.txt
```