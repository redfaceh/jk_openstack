## 1. 의존성 설치

### 1) python 관련 패키지 설치
```bash
$ sudo yum install python-devel libffi-devel gcc openssl-devel libselinux-python
```

### 2) python 가상 환경 설치
```bash
$ sudo yum install python-virtualenv
```
```bash
$ virtualenv ~/kolla
$ source ~/kolla
```

```bash
$ pip install -U pip
```

## 2. kolla-ansible 환경 구성

### 1) kolla-ansible 파일 설치
```bash
$ pip install kolla-ansible==8.0.1
```

### 2) kolla 설정 파일  
```bash
$ sudo mkdir -p /etc/kolla
$ sudo chown $USER:$USER /etc/kolla
$ cp -r ~/kolla/share/kolla-ansible/etc_examples/kolla/* /etc/kolla
```



## 3. ansible 설정

### 1) ansible.cfg 파일
```bash
$ sudo vi /etc/ansible/ansible.cfg
[defaults]
host_key_checking=False
pipelining=True
forks=100
```

### 2) 키 기반 인증
```bash
$ ssh-keygen
$ ssh-copy stack@localhost
```

### 3) sudo 사용자 설정
```bash
$ sudo vi /etc/sudoers.d/stack
stack    ALL=(ALL:ALL)  NOPASSWD: ALL
```

### 4) 인벤토리 
```bash
$ cp ~/kolla/share/kolla-ansible/ansible/inventory/* .
```
all-in-one은 인벤토리 수정 불필요


## 4. yaml 파일 수정

### 1) password.yml 패스워드 생성
```bash
$ kolla-genpwd
```

### 2) globals.yml 수정
```bash
$ vi /etc/kolla/globals.yml

```

## 5. 배포
```bash
$ kolla-ansible -i all-in-one bootstrap-servers
```
```bash
$ kolla-ansible -i all-in-one prechecks
```
```bash
$ kolla-ansible -i all-in-one pull
```
```bash
$ kolla-ansible -i all-in-one deploy
```